<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Robot;

class RobotsController extends AbstractController
{
    /**
     * @Route("/robots", name="list")
     */ 
    public function index(): Response
    {
        $robots = $this->getDoctrine()->getRepository(Robot::class)->findAll();
        return $this->render('robot/list.html.twig', [
            'robots' => $robots,
        ]);
    }

    /**
     * @Route("/robot/edit/{id}", name="edit_robot")
     */ 
    public function edit(Request $request, $id = 0): Response
    {
        $robot = $this->getDoctrine()->getRepository(Robot::class)->findOneBy(array('id' => $id));
        if($request->isMethod('POST')){
            $this->add($request, $id);
            return $this->redirectToRoute('list');
        }
        return $this->render('robot/edit.html.twig', [
            'robot' => $robot,
        ]);
    }

    /**
     * @Route("/robot/add", name="add_robot")
     */ 
    public function add(Request $request, $id = 0): Response
    {
        if($request->isMethod('POST'))
        {
            $data = $request->request->all();
            $RobotRepository = $this->getDoctrine()->getRepository(Robot::class);
            $robot = $RobotRepository->find($id);
            if(empty($robot)) {
                $robot = new Robot();
            }
            $robot->setName($data['name']);
            $robot->setType($data['type']);
            $robot->setStrength($data['strength']);
            if($id >= 1){
                $robot->setStatus($robot->getStatus());
            }else{
                $robot->setStatus(1);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($robot);
            $entityManager->flush();
            $this->addFlash('success', 'Sikeres rögzítés!');
            return $this->redirectToRoute('list');
        }else{
            return $this->render('robot/add.html.twig');
        }
    }
    
    /**
     * @Route("/robot/updateStatus/{id}", name="update_status")
     */
    public function updateStatus($id = 0): Response 
    {
        $RobotRepository = $this->getDoctrine()->getRepository(Robot::class);
        $robot = $RobotRepository->find($id);
        $robot->setStatus(!$robot->getStatus());
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('info', 'Sikeresen átállította a státuszt!');
        return $this->redirectToRoute('list');
    }

    public function robotBattle(Request $request) {
        $robots = $this->getDoctrine()->getRepository(Robot::class)
            ->selectStrongerByIds($request->query->get('robot_ids'));
        if(!$robots) { return new NotFoundHttpException(); }
        return $this->json($robots, 200, ["Content-Type" => "application/json"]);
    }

}
